# **_Milestone 1: Planning Report_**

## **_Jaskarn Singh, Sam halligan, Cameron Martin_**

### **Our Project**

   > For our project we plan to make a simple web application. This application will pull information from Facebook. This means we will be using the Facebook API. As a group we decided this API was something we were interested in and wanted to learn more about. Especially since we are all advid users of Facebook. Further this gives us a chance to learn more about the code and processes used. In the Web design and application of Facebook.

### **Tools used and Why?**

#### **Facebook Messenger**

   > We will be using Facebook Messenger as our main form of communication. This is mainly due to the ease of use and accessibility. We all use messenger and always check it. Logically this is a better method then slack. However because we are required to use slack. I will make sure we copy paste all information and conversation about the group project to the slack channel.

#### **Slack**

   > Although I said Facebook Messenger would be our main form of communication. During class or times when we are at the ploytechnic we will make sure to use slack and share any relevant information/resources we find there. Slack has a lot of unique functions like being able to pin posts/links. We will be using this function to keep track of important information and resorces for our project.

### **Wireframe**

   > We have come up with a simple wireframe showcasing our application. We as a group acknowledge we are not so good with programming. So we decided to stick with a simplistic program that will cover the requirements of the assessment. Looking briefly over the funcions and coding required. And more at what we want out of the app. This relates to what we hope to achieve through this application.

#### **Key Information about the application**

1. Functions
2. Terms and Conditions
3. API Knowledge
4. ASP.NET CORE and API
5. Authorization
6. User Interface Considerations
7. Aesthetics 
8. Components
9. Updating data or refreshing

   > All the above will be researched and added to our collected information. Allowing any one of us to be able to complete any task.

#### **Planning and Key Components**

1. Starting off we will need one of us to make each of the main three components. This relates to the two main pages and the pop-up page. 
    1. Create the Login Page.
    2. Create the Home Page.
    3. Create the Login Pop-up Page.
    4. 
2. Next another one of us will make sure the images/layout of the pages match the wireframe.
3. Images and Colour scheme will match Facebook. Images have already been gathered.
4. Interactive buttons and making sure the web application flows and is functional.
5. Code for the web app to be compatible on multiple platforms.
6. Integrate Facebook API into the web app.
7. Incorporate key gui elements into designing the Web app.
8. Data can be refreshed through the refresh button.


[Wireframe for our App](https://drive.google.com/a/bcs.net.nz/file/d/0B8YB6--euYpOTDkwUG9sbXVEZTA/view?usp=sharing)

### **Roles of Group Members**

